package com.bitshifter.weatherwizard;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Preferences extends AppCompatActivity {

    TextView currentCity;
    EditText newCity;
    Button changeCity;
    DataBaseHandler db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);
        currentCity = (TextView) findViewById(R.id.tvCurrentCity);
        newCity = (EditText) findViewById(R.id.etChangeCity);
        changeCity = (Button) findViewById(R.id.bChangeCity);
        db = new DataBaseHandler(this,null,null,1);
        if(db.getCityName().equals(""))
            currentCity.setText("Choose a City First");
        else
            currentCity.setText("Current City is "+db.getCityName());
    }

    public void changeCurrentCity(View view){
        String city;
        city = newCity.getText().toString();
        if(city != "") {
            db.updateCity(city);
            Toast.makeText(this, "City successfully changed to "+city, Toast.LENGTH_LONG);
            currentCity.setText("Current City is "+db.getCityName());
          //  Intent intent = new Intent(this,HomeScreen.class);
          // startActivity(intent);
        }
        else
            Toast.makeText(this,"Please write a valid city name",Toast.LENGTH_LONG);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_preferences, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }
}
