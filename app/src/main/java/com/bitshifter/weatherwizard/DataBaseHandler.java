package com.bitshifter.weatherwizard;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by rohit on 18/8/15.
 */
public class DataBaseHandler extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;
    private static final String DB_NAME ="WEATHER.db";
    private static final String TABLE_CITY = "CITY";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_CITY_NAME = "city_name";
    public DataBaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DB_NAME, factory, DB_VERSION);
    }

    //Will fetch data from database
    public String getCityName(){
        String city = "";
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_CITY + " WHERE 1";
        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToFirst();
        city = cursor.getString(cursor.getColumnIndex(COLUMN_CITY_NAME));
        return  city;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE "+ TABLE_CITY +" ("+
                COLUMN_ID + " INTEGER PRIMARY KEY, "+
                COLUMN_CITY_NAME + "  TEXT" +
                ");";
        db.execSQL(query);
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID,"1");
        values.put(COLUMN_CITY_NAME,"");
        db.insert(TABLE_CITY,null,values);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //db.execSQL("DROP TABLE IF EXISTS "+ TABLE_CITY);
    }

    public void updateCity(String City){
        ContentValues values = new ContentValues();
        //values.put(COLUMN_ID,"1");
        values.put(COLUMN_CITY_NAME,City);
        SQLiteDatabase db = getWritableDatabase();
        db.update(TABLE_CITY, values, COLUMN_ID +"= 1",null);
    }
}
