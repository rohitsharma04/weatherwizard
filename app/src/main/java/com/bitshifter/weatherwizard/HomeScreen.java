package com.bitshifter.weatherwizard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

public class HomeScreen extends AppCompatActivity {

    TextView tv;
    DataBaseHandler db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        db = new DataBaseHandler(this,null,null,1);
        tv = (TextView) findViewById(R.id.textView);
        setData();
    }
    public void setData(){
        OpenWeatherRestClient.get(db.getCityName(), null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                //Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_LONG).show();
                tv.setText(response.toString());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.home_screen_preferences:
                intent = new Intent(this,Preferences.class);
                startActivity(intent);
                return true;
            case R.id.home_screen_about_us:
                intent = new Intent(this,AboutUs.class);
                startActivity(intent);
                return true;
            case R.id.home_screen_refresh:
                setData();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
