package com.bitshifter.weatherwizard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class SplashScreen extends AppCompatActivity {

    private String TAG = getClass().getName();
    private TextView textView;
    DataBaseHandler db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        db = new DataBaseHandler(this,null,null,1);
        //setting textview
        textView = (TextView) findViewById(R.id.appName);
        callInterpolator();
        //Starting new Activity after 3 seconds
        Thread timer = new Thread(){
            @Override
            public void run(){
                try{
                    sleep(3000);
                }catch (InterruptedException e){
                    Log.e(TAG, e.toString());
                }finally {
                    startHomeScreenActivity();
                }
            }
        };
        timer.start();

    }
    public void callInterpolator() {
        Animation tvAnim = AnimationUtils.loadAnimation(this, R.anim.interpolator_anim);
        textView.startAnimation(tvAnim);
    }
    void startHomeScreenActivity(){
        Intent intent;
        if(db.getCityName().equals(""))
            intent = new Intent(this,Preferences.class);
        else
            intent = new Intent(this,HomeScreen.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



        return super.onOptionsItemSelected(item);
    }
}
